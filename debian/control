Source: nodebox-web
Section: python
Priority: optional
Maintainer: Serafeim Zanikolas <sez@debian.org>
Build-Depends: debhelper (>= 7.0.50~), python (>= 2.6.6-3~)
Standards-Version: 3.9.2
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Vcs-Git: https://salsa.debian.org/python-team/packages/nodebox-web.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/nodebox-web
Homepage: http://nodebox.net/code/index.php/Web

Package: python-nodebox-web
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, python-beautifulsoup,
         python-soappy, python-feedparser, python-simplejson
Provides: ${python:Provides}
Description: collection of web-related Python modules
 Nodebox Web is a collection of Python modules to get content from the web.
 One can query Yahoo! and Google for links, images, news and spelling
 suggestions, read RSS and Atom newsfeeds, retrieve articles from Wikipedia,
 collect quality images from morgueFile or Flickr, browse through HTML
 documents, clean up HTML, validate URLs, and create GIF images from math
 equations using mimeTeX.
 .
 The library uses a caching mechanism that stores things you download from the
 web, so they can be retrieved faster the next time. Many of the services also
 work asynchronously.
